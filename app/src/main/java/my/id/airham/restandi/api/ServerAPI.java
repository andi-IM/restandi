package my.id.airham.restandi.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerAPI {
    private static final String baseUrl = "tokonya-andi.000webhostapp.com";
    private static Retrofit retrofit = null;

    public static Retrofit ServerApi() {
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
        }
        return retrofit;
    }
}
