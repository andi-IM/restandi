package my.id.airham.restandi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import my.id.airham.restandi.R;
import my.id.airham.restandi.model.User;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    Context context;
    private List<User> userList;

    public UserAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_user, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.txtId.setText(userList.get(position).getId());
        holder.txtUser.setText(userList.get(position).getUsername());
        holder.txtPass.setText(userList.get(position).getPassword());
        holder.txtMail.setText(userList.get(position).getEmail());
        holder.txtPhoneNo.setText(userList.get(position).getPhone());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        TextView txtId, txtUser, txtPass, txtMail, txtPhoneNo;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);

            txtId = itemView.findViewById(R.id.txtId);
            txtUser = itemView.findViewById(R.id.txtUname);
            txtPass = itemView.findViewById(R.id.txtPass);
            txtMail = itemView.findViewById(R.id.txtMail);
            txtPhoneNo = itemView.findViewById(R.id.txtPhoneNo);

        }
    }
}
