package my.id.airham.restandi.api;

import java.util.List;

import my.id.airham.restandi.model.User;
import retrofit2.http.GET;

public interface SelectAPI {

    @GET
    public List<User> getUser();
}
